package com.nestforce.apps.remotedroid;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class UsersActivity extends AppCompatActivity {

    private Context mContext;
    private ListView mLvUsers;
    private Button mBtnUsersAdd;
    private UsersDB mUsersDB;

    private void init() {
        mContext = this;
        mLvUsers = (ListView) findViewById(R.id.id_lvUsers);
        mBtnUsersAdd = (Button) findViewById(R.id.id_btnUsersAdd);

        mUsersDB = new UsersDB(mContext);

        final ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
                mContext,android.R.layout.simple_list_item_1
        );

        ContentValues[] users = mUsersDB.selectAllUsers();
        for (ContentValues user : users){
            listAdapter.add(user.get("username").toString());
        }

        mLvUsers.setAdapter(listAdapter);

        mLvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                final String username = mLvUsers.getItemAtPosition(pos).toString();

                DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int button) {
                        switch (button){
                            case (-1): //edit
                                Intent intent = new Intent(mContext, UsersAddActivity.class);
                                intent.putExtra("username", username);
                                startActivity(intent);
                                break;
                            case (-2): //delete
                                mUsersDB.removeUser(username);
                                //refresh
                                finish();
                                startActivity(getIntent());
                        }
                    }
                };

                Utils.dialogChoice(mContext,username,null,null,
                        getString(R.string.str_btnEdit),
                        dialogListener,
                        getString(R.string.str_btnDelete),
                        dialogListener
                );


            }
        });
        View.OnClickListener btnListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = view.getId();

                switch (id){
                    case (R.id.id_btnUsersAdd):
                        Intent intent = new Intent(mContext,UsersAddActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        };

        mBtnUsersAdd.setOnClickListener(btnListener);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        
        init();
    }

}
