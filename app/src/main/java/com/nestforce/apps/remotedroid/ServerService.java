package com.nestforce.apps.remotedroid;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;

public class ServerService extends Service {

    private HTTPServer mHttpServer;
    private int mHttpPort = 8080;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private Context mContext;

    public ServerService(Context context) {
        super();
        mContext = context;
        Log.d(Utils.APP_IDENTIFIER, "ServerService: INSTANTIATED");
    }

    public ServerService(){
        super();
        Log.d(Utils.APP_IDENTIFIER, "ServerService Simple: INSTANTIATED");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        mContext = getApplicationContext();

        Log.d(Utils.APP_IDENTIFIER, "ServerService: Server Started");

        Intent broadcastIntent = new Intent("com.nestforce.apps.remotedroid.ActivityRecognition.ServerMonitor");
        broadcastIntent.putExtra("action","started");


        try {
            mHttpServer = new HTTPServer(mHttpPort, new API(mContext), mContext);
            mHttpServer.start();
        } catch (Exception e){
            Log.e(Utils.APP_IDENTIFIER, "ServerService ".concat(e.getMessage()));
        }

        sendBroadcast(broadcastIntent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(Utils.APP_IDENTIFIER, "ServerService DESTROYED");
        Intent broadcastIntent = new Intent("com.nestforce.apps.remotedroid.ActivityRecognition.ServerMonitor");
        broadcastIntent.putExtra("action","destroyed");

        sendBroadcast(broadcastIntent);
        mHttpServer.stop();
    }
}
