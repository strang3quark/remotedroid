package com.nestforce.apps.remotedroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UsersAddActivity extends AppCompatActivity {
    private Context mContext;
    private EditText mEtUsername, mEtPassword, mEtPasswordConfirm;
    private Button mBtnUserAdd;

    private UsersDB mUsersDB;
    private boolean newUser;

    private void init(){
        mContext = this;
        mEtUsername = (EditText) findViewById(R.id.id_etUsername);
        mEtPassword = (EditText) findViewById(R.id.id_etPassword);
        mEtPasswordConfirm = (EditText) findViewById(R.id.id_etPasswordConfirm);
        mBtnUserAdd = (Button) findViewById(R.id.id_btnUserAdd);

        mUsersDB = new UsersDB(mContext);

        Intent intent = getIntent();
        if (intent.hasExtra("username")) {
            newUser = false;
            mEtUsername.setText(intent.getStringExtra("username"));
            mEtUsername.setEnabled(false);
            setTitle(getString(R.string.ttl_activityUsersEdit));
        }else{
            newUser = true;
        }


        View.OnClickListener btnListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = view.getId();

                if (mEtPassword.getText().toString().equals(mEtPasswordConfirm.getText().toString())){
                    String username = mEtUsername.getText().toString();
                    String password = mEtPassword.getText().toString();
                    if (username.equals("") || password.equals("")){
                        Utils.dialogMessage(mContext,getString(R.string.str_dlgError),
                                getString(R.string.str_dlgMissingFields), null, null);
                    }

                    boolean success = false;
                    if (!newUser){
                        long rowsAffected = mUsersDB.updateUserPassword(username,password);
                        if (rowsAffected > 0){
                            success = true;
                        }
                    }else{
                        long uid = mUsersDB.insertUser(username,password);
                        if (uid > -1){
                            success = true;
                        }
                    }

                    if (success){
                        Intent intent = new Intent(mContext,UsersActivity.class);
                        startActivity(intent);
                    }else{
                        Utils.dialogMessage(mContext,getString(R.string.str_dlgError),
                                getString(R.string.str_dlgDatabaseError), null, null);
                    }
                }else{
                    Utils.dialogMessage(
                            mContext,
                            getString(R.string.str_dlgError),
                            getString(R.string.str_dlgPasswordsNotMatch),
                            ContextCompat.getDrawable(mContext,android.R.drawable.ic_dialog_alert),
                            null
                    );
                }
            }
        };

        mBtnUserAdd.setOnClickListener(btnListener);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_add);

        init();
    }
}
