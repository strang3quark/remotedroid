package com.nestforce.apps.remotedroid;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class About extends AppCompatActivity {

    private TextView mTvInfo;

    private void init(){
        mTvInfo = (TextView) findViewById(R.id.id_tvInfo);

        String text =
                getString(R.string.app_name)+"\n"
                +getString(R.string.about_version)+"\n"
                +getString(R.string.about_developers)+"\n"
                +getString(R.string.about_webpage);
        mTvInfo.setText(text);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        init();
    }
}
