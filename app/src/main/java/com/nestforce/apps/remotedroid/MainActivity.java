package com.nestforce.apps.remotedroid;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.IOException;
import java.lang.reflect.Method;


public class MainActivity extends AppCompatActivity {

    private Context mContext;
    private Switch mSwitchWebServer;
    private TextView mTvFeedback;
    private TextView mTvMain, mTvSub;

    private Integer mHttpPort;

    private Intent mServiceIntent;
    private ServerService mServerService;

    private void init() {
        mContext = this;

        mHttpPort = 8080;

        //Initialize the Service
        mServerService = new ServerService(mContext);
        mServiceIntent = new Intent(mContext, mServerService.getClass());

        //Gui Items
        mSwitchWebServer = (Switch) findViewById(R.id.id_switchWebServer);
        mSwitchWebServer.setChecked(isServiceRunning(mServerService.getClass()));

        mTvMain = (TextView) findViewById(R.id.id_tvMain);
        mTvSub = (TextView) findViewById(R.id.id_tvSub);


        mSwitchWebServer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               WebServerToggle(b);
            }
        });

        //Verify network status manually at start
        changedWifiStatus(isWifiConnected());

        BroadcastReceiver mBRConnectionChanged = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                changedWifiStatus(isWifiConnected());
            }
        };

        IntentFilter broadcastListenTo = new IntentFilter();
        broadcastListenTo.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastListenTo.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        broadcastListenTo.addAction("android.net.wifi.STATE_CHANGE");
        registerReceiver(mBRConnectionChanged, broadcastListenTo);

        BroadcastReceiver mBRServerStatus = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(Utils.APP_IDENTIFIER, "MainActivity -> BroadCastReceiver: Status changed");
            }
        };

        IntentFilter broadcastServerListenTo = new IntentFilter();
        broadcastServerListenTo.addAction("com.nestforce.apps.remotedroid.ServerService");
        registerReceiver(mBRServerStatus, broadcastServerListenTo);
    }

    protected void askPermissions(){
        MultiplePermissionsListener dialogMultiplePermissionsListener =
                DialogOnAnyDeniedMultiplePermissionsListener.Builder
                        .withContext(mContext)
                        .withTitle(getString(R.string.str_askpermission_title))
                        .withMessage(getString(R.string.str_askpermission_content))
                        .withButtonText(android.R.string.ok)
                        .build();

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.READ_SMS
                ).withListener(dialogMultiplePermissionsListener
        ).check();
    }

    protected void WebServerStart(){
        askPermissions();
        startService(mServiceIntent);

        mTvMain.setText(getString(R.string.str_tvMain_Running));
        String ip = Utils.wifiIpAddress();
        String port = String.valueOf(mHttpPort);
        mTvSub.setText(getString(R.string.str_tvSub_Running)+ip+":"+port);

    }
    protected void WebServerStop(){
        stopService(mServiceIntent);

        mTvMain.setText(getString(R.string.str_tvMain_NotRunning));
        mTvSub.setText(getString(R.string.str_tvMain_NotRunning));
    }

    protected void WebServerToggle(boolean start){
        if (start){
            WebServerStart();
        } else {
            WebServerStop();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.id_menu_main_users){
            //Start user management activity
            Intent intent = new Intent(mContext, UsersActivity.class);
            startActivity(intent);
        }else if (id == R.id.id_menu_main_about){
            Intent intent = new Intent(mContext, About.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void changedWifiStatus(boolean isWifiConnected) {
        if (isWifiConnected){
            mTvMain.setText(getString(R.string.str_tvMain_NotRunning));
            mTvSub.setText(getString(R.string.str_tvSub_NotRunning));
        }else{
            mTvMain.setText(getString(R.string.str_tvMain_NoWifi));
            mTvSub.setText(getString(R.string.str_tvSub_NoWifi));
            mSwitchWebServer.setChecked(false);
        }
        mSwitchWebServer.setEnabled(isWifiConnected);
    }

    public boolean isWifiConnected(){
        ConnectivityManager cm = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isWifiConnected = false;
        boolean isHotspotEnabled = false;
        if (activeNetwork != null){
            isWifiConnected = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        }

        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        try
        {
            Method method = wm.getClass().getDeclaredMethod("isWifiApEnabled");
            method.setAccessible(true); //in the case of visibility change in future APIs
            isHotspotEnabled = (Boolean) method.invoke(wm);
        }
        catch (Throwable error)
        {
            Log.e(Utils.APP_IDENTIFIER, "MainActivity -> isWifiConnected ".concat(error.getMessage()));
        }


        return isWifiConnected || isHotspotEnabled;
    }

    private boolean isServiceRunning(Class<?> serviceClass){
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i (Utils.APP_IDENTIFIER, "MainActivity -> isServiceRunning? TRUE");
                return true;
            }
        }
        Log.i (Utils.APP_IDENTIFIER, "MainActivity -> isServiceRunning? FALSE");
        return false;
    }

    @Override
    protected void onDestroy() {
        try {
            stopService(mServiceIntent);
            Log.i (Utils.APP_IDENTIFIER, "MainActivity -> OnDestroy");
        }catch (Exception e){
            Log.i (Utils.APP_IDENTIFIER, "MainActivity -> OnDestroy ".concat(e.getMessage()));
        }
        super.onDestroy();
    }


}
