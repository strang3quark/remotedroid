package com.nestforce.apps.remotedroid;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContactsUtil {

    private Context mContext;

    public ContactsUtil(Context mContext) {
        this.mContext = mContext;
    }

    private List<HashMap<String, String>> getContacts(String where, String[] whereArgs){
        List<HashMap<String,String>> ret = new ArrayList<HashMap<String, String>>();
        Cursor c = mContext.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                where,
                whereArgs,
                ContactsContract.Contacts.DISPLAY_NAME.concat(" ASC")
        );

        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                HashMap<String,String> contact = new HashMap<String,String>();

                contact.put("id",c.getString(c.getColumnIndex(ContactsContract.Contacts._ID)));
                contact.put("name",c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));

                if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = mContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", new String[]{contact.get("id")}, null);
                    while (pCur.moveToNext()) {
                        String phoneNr = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contact.put("phonenr",phoneNr);
                    }
                    pCur.close();
                }

                ret.add(contact);
            }
        }
        return ret;
    }

    public HashMap<String,String> getContactById(int id){
        String sId = String.valueOf(id);
        List<HashMap<String,String>> contacts = getContacts("_id=?",new String[]{sId});
        if (contacts.size() > 0){
            return contacts.get(0);
        }else{
            return null;
        }
    }

    public String getContactId(String phoneNumber) {
        if (phoneNumber == "")
            return null;
        ContentResolver cr = mContext.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup._ID }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactID = null;
        if (cursor.moveToFirst()) {
            contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup._ID));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactID;
    }

    List<HashMap<String, String>> getAllContacts(){
        return getContacts(null,null);
    }
}
