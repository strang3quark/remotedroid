package com.nestforce.apps.remotedroid;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;

public final class Utils {

    static final String APP_IDENTIFIER = "RemoteDroid";

    static String wifiIpAddress() {
        //ADAPTED FROM: http://stackoverflow.com/questions/9573196/how-to-get-the-ip-of-the-wifi-hotspot-in-android
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                if (intf.getName().contains("wlan") || intf.getName().contains("ap")) {
                    Log.d("WIFI_IP", intf.getName());
                    for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr
                            .hasMoreElements();) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()
                                && (inetAddress.getAddress().length == 4)) {
                            Log.d("WIFI_IP_ADDRESS", inetAddress.getHostAddress());
                            return inetAddress.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(APP_IDENTIFIER, "Utils -> wifiIpAddress".concat(ex.getMessage()));
        }
        return null;
    }

    static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0x100 | 0xFF & messageDigest[i]).substring(1));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    static void dialogChoice(
            Context context,
            String title,
            String message,
            Drawable icon,
            String positiveButtonText,
            DialogInterface.OnClickListener positiveListener,
            String negativeButtonText,
            DialogInterface.OnClickListener negativeListener
    ){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                //.setPositiveButton(android.R.string.yes,positiveListener)
                //.setNegativeButton(android.R.string.no,negativeListener)
                .setPositiveButton(positiveButtonText,positiveListener)
                .setNegativeButton(negativeButtonText,negativeListener)
                .setIcon(icon)
                .show();

        /*icon = android.R.drawable.ic_dialog_alert*/
    }

    static void dialogMessage(Context context, String title, String message, Drawable icon, DialogInterface.OnClickListener listener){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setIcon(icon)
                .setNeutralButton(android.R.string.ok,listener)
                .show();
    }
}
