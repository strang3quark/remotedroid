package com.nestforce.apps.remotedroid;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.iki.elonen.NanoHTTPD;

public class API {

    private Context mContext;
    private SMSUtils mSms;
    private ContactsUtil mContacts;
    private Gson mGson;

    private UsersDB mUsersDB;

    public API(Context pContext){
        mContext = pContext;
        mSms = new SMSUtils(mContext);
        mContacts = new ContactsUtil(mContext);
        mGson = new Gson();

        mUsersDB = new UsersDB(mContext);
    }

    public String handle(NanoHTTPD.IHTTPSession session) {
        String uri = session.getUri();
        Map<String, String> parms = session.getParms();
        Map<String,String> headers = session.getHeaders();
        String ret = "{\"error\":\"invalid api call\"}";

        Log.d(Utils.APP_IDENTIFIER, "API -> handle URI=".concat(uri));

        boolean validUser = validateUser(headers);

        if (!validUser){
            return "{\"validauth\":\"false\"}";
        }

        switch (uri) {
            /*IP:PORT/sms/threads*/
            case ("/sms/threads"):
                ret = getSMSThreads();
                break;
            /*IP:PORT/sms/thread?threadid=XX*/
            /*IP:PORT/sms/thread?address=XX*/
            case ("/sms/thread"):
                ret = getSMSThread(parms);
                //markSMSRead(parms);
                break;
            /*IP:PORT/sms/unread*/
            /*IP:PORT/sms/unread?threadid=XX*/
            case ("/sms/unread"):
                ret = getSMSUnread(parms);
                break;
            /*IP:PORT/sms/markread?threadid=XX*/
            case ("/sms/markread"):
                ret = markSMSRead(parms);
                break;
            /*IP:PORT/sms/send?address=XXXXXXXXX&content=XXDDXX*/
            case ("/sms/send"):
                ret = sendSMS(parms);
                break;
            case ("/sms/send/status"):
                ret = sentSmsStatus();
                break;
            /*IP:PORT/contacts*/
            case ("/contacts"):
                ret = getContacts();
                break;
            /*IP:PORT/contact/get?id=XXX*/
            /*IP:PORT/contact/get?address=XXX*/
            case ("/contact/get"):
                ret = getContact(parms);
                break;
            //if we got here we have a valid session
            case ("/auth"):
                ret = "{\"validauth\":\"true\"}";
                break;
        }
        return ret;
    }

    private boolean validateUser(Map<String, String> headers) {
        boolean valid = false;
        if (headers.containsKey("auth-user") && headers.containsKey("auth-password")){
            int uid = mUsersDB.validateUser(headers.get("auth-user"),headers.get("auth-password"));
            valid = (uid > -1);
        }
        return valid;
    }

    private String sentSmsStatus() {
        HashMap<String,String> status = mSms.lastSentSmsStatus();
        return mGson.toJson(status);
    }

    private String sendSMS(Map<String, String> parms) {
        //TODO: not looking good
        if (!(parms.containsKey("address") && parms.containsKey("content"))){
           return "{\"ERROR\":\"Address/Content parameters missing\"}"; //TODO: make constants
        }
        if (parms.containsKey("method")){
            if (parms.get("method").equals("intent")){
                mSms.sendSMSIntent(parms.get("address"), parms.get("content"));
                return "{\"SENDSMS\":\"DONE\"}";
            }
        }
        mSms.sendSMS(parms.get("address"), parms.get("content"));
        return "{\"SENDSMS\":\"DONE\"}";
    }

    private String markSMSRead(Map<String, String> parms) {
        if (parms.containsKey("threadid")){
            int threadid = Integer.parseInt(parms.get("threadid"));
            mSms.markThreadRead(threadid);
            return "{\"MARKSMSREAD\":\"DONE\"}";
        }else{
            return "{\"ERROR\":\"Invalid parameters\"}";
        }
    }

    private String getContact(Map<String, String> parms) {
        HashMap<String, String> contact;
        if (parms.containsKey("id")) {
            contact = mContacts.getContactById(Integer.parseInt(parms.get("id")));
        }
        else if (parms.containsKey("address")) {
            String contactID = mContacts.getContactId(parms.get("address"));
            if (contactID == null){
                contact = new HashMap<String , String>();
                contact.put("NOTFOUND", "Contact not found in database");
            } else {
                contact = mContacts.getContactById(Integer.parseInt(contactID));
            }
        }else{
            contact = new HashMap<>();
            contact.put("ERROR", "Invalid Parameters");
        }
        return mGson.toJson(contact);
    }

    private String getContacts() {
        String ret = "";
        List<HashMap<String,String>> contact = mContacts.getAllContacts();
        ret = mGson.toJson(contact);
        return ret;
    }

    private String getSMSThread(Map<String, String> parms) {
        String result = "{\"ERROR\":\"Invalid parameters\"}";

        int limitStart = 0;
        int limitEnd = 10;

        if (parms.containsKey("start") && parms.containsKey("end")){
            try {
                limitStart = Integer.parseInt(parms.get("start"));
                limitEnd = Integer.parseInt(parms.get("end"));
            }catch(Exception e){
                Log.e("API ERROR:", "Cannot convert start and end values to INT");
            }
        }

        if (parms.containsKey("threadid")){
            int threadid = Integer.parseInt(parms.get("threadid"));
            result = mGson.toJson(mSms.getThreadMessages(threadid, limitStart, limitEnd));
        }else if (parms.containsKey("address")) {
            String address = parms.get("address");
            result = mGson.toJson(mSms.getThreadMessagesByAddress(address, limitStart, limitEnd));
        }else if (parms.containsKey("contactid")) {
            try {
                int contactid = Integer.parseInt(parms.get("contactid"));
                result = mGson.toJson(mSms.getThreadMessagesByContactID(contactid, limitStart, limitEnd));
            }catch (NumberFormatException e){
                return "{\"ERROR\":\"contactid must be a valid number\"}";
            }
        }
        return result;
    }

    private String getSMSThreads(){
        List<HashMap<String,String>> threads = mSms.getThreadList();
        return mGson.toJson(threads);
    }

    private String getSMSUnread(Map<String, String> parms) {
        if (parms.containsKey("threadid")){
            int threadid = Integer.parseInt(parms.get("threadid"));
            return mGson.toJson(mSms.getUnreadMessages(threadid));
        } else {
            return mGson.toJson(mSms.getUnreadMessages());
        }
    }
}
