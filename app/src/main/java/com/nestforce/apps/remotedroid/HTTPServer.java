package com.nestforce.apps.remotedroid;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.StringTokenizer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response;

public class HTTPServer extends NanoHTTPD{

    private API mAPI;
    private Context mContext;

    public HTTPServer(int port, API pAPI, Context context) {
        super(port);
        mAPI = pAPI;
        mContext = context;
    }

    public HTTPServer(String hostname, int port, API pAPI) {
        super(hostname, port);
        mAPI = pAPI;
    }

    @Override
    public Response serve(IHTTPSession session) {
        Response response;
        String uri = session.getUri();

        if (uri.equals("/")){
            uri = "/index.html";
        }
        String mimetype = getMimeType(uri);
        Log.d("HTTPServer","MimeType: "+mimetype);
        if (mimetype != null) {
            //Handle text files
            if (mimetype.contains("text/") || mimetype.contains("image/")){
                response = newFixedLengthResponse(getTextFile(uri.substring(1)));
            } else { //handle binary files
                FileInputStream fcontent = getBinaryFile(uri.substring(1));
                response = NanoHTTPD.newChunkedResponse(Response.Status.OK,mimetype,fcontent);
            }
            response.setMimeType(mimetype);
        } else { //Let API Object handle the request
            response = newFixedLengthResponse(mAPI.handle(session));
            response.setMimeType("application/json");
        }

        //Allow CORS
        response.addHeader("Access-Control-Allow-Origin","*");
        response.addHeader("Access-Control-Allow-Headers","auth-user, auth-password");
        return response;
    }

    /* Get binary file from resources */
    private FileInputStream getBinaryFile(String resourceLocation){
        FileInputStream fis = null;
        try {
            AssetFileDescriptor fd = mContext.getResources().getAssets().openFd(resourceLocation);
            fis = fd.createInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fis;
    }

    /* Get text file from resources and returns as String */
    private String getTextFile(String resourceLocation){
        try {
            InputStream is = mContext.getResources().getAssets().open(resourceLocation);
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString("UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    /* Get's the mimetype based on URI
        Returns null if uri is not a file
     */
    private String getMimeType(String uri){
        try {
            //Initialize hashmap
            HashMap<String, String> validExtensions = new HashMap<String, String>();
            validExtensions.put("mp3", "audio/mpeg");
            validExtensions.put("js", "text/javascript");
            validExtensions.put("css", "text/css");
            validExtensions.put("html", "text/html");
            validExtensions.put("ico", "image/x-icon");

            //Check if URI is file (needs to have a dot)
            int dotLocation = uri.lastIndexOf(".");
            if (dotLocation > -1) {
                //Get the extension
                String ext = uri.substring(dotLocation+1); //+1 to remove the dot
                Log.d(Utils.APP_IDENTIFIER, "HTTPServer -> getMimeType EXTENSION=".concat(ext));
                if (validExtensions.containsKey(ext)) {
                    return validExtensions.get(ext);
                }
            }
        } catch (Exception e){
            Log.e(Utils.APP_IDENTIFIER, "HTTPServer -> getMimeType ".concat(e.getMessage()));
        }
        return null;
    }


}
