package com.nestforce.apps.remotedroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class UsersDB extends SQLiteOpenHelper {

    private final static String UTILIZADORES_DB_NAME = "users.db";
    private final static int UTILIZADORES_DB_VERSION = 1;

    private final static String TABLE_UTILIZADORES = "users";
    private final static String TABLE_UTILIZADORES_COL_ID = "_id";
    private final static String TABLE_UTILIZADORES_COL_USERNAME = "username";
    private final static String TABLE_UTILIZADORES_COL_PASSWORD = "password";

    private final static String CREATE_TABLE_UTILIZADORES = "CREATE TABLE IF NOT EXISTS " +
            TABLE_UTILIZADORES + "(" +
            TABLE_UTILIZADORES_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            TABLE_UTILIZADORES_COL_USERNAME + " TEXT NOT NULL," +
            TABLE_UTILIZADORES_COL_PASSWORD + " TEXT NOT NULL" + ");";

    private final static String DROP_TABLE_UTILIZADORES = "DROP TABLE IF EXISTS " + TABLE_UTILIZADORES + ";";

    public UsersDB(Context pContext) {
        super(
                pContext,
                UTILIZADORES_DB_NAME,
                null,
                UTILIZADORES_DB_VERSION
        );
    }

    private void installDB(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_UTILIZADORES);
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.toString() +
                    " FAILED@installDB\n" + CREATE_TABLE_UTILIZADORES
            );
        }
    }

    private void reinstallDB(SQLiteDatabase db, int oldVersion) {
        /*
            TODO: depends on the changes in the next version
            For now it will drop all the database and start fresh
         */
        try {
            db.execSQL(DROP_TABLE_UTILIZADORES);
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.toString() + " FAILED@reinstallDB\n" +
                    DROP_TABLE_UTILIZADORES);
        }

        installDB(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        installDB(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        reinstallDB(db, oldVersion);
    }

    //---

    public ContentValues[] selectAllUsers() {
        ContentValues[] users;
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            String q = "SELECT * FROM " + TABLE_UTILIZADORES + ";";
            String filters[] = null;

            Cursor c = db.rawQuery(q, filters);
            int resultCount = c.getCount();
            users = new ContentValues[resultCount];
            c.moveToFirst();
            int idx = 0;
            while (!c.isAfterLast()) {
                long tbId = c.getLong(c.getColumnIndex(TABLE_UTILIZADORES_COL_ID));
                String tbUsername = c.getString(c.getColumnIndex(TABLE_UTILIZADORES_COL_USERNAME));
                String tbPassword = c.getString(c.getColumnIndex(TABLE_UTILIZADORES_COL_PASSWORD));

                ContentValues user = new ContentValues();
                user.put("id", tbId);
                user.put("username", tbUsername);
                user.put("password", tbPassword);

                users[idx] = user;
                idx++;
                c.moveToNext();
            }
            db.close();
        } catch (Exception e) {
            users = null;

            Log.e(this.getClass().getName(), e.toString() +
                    " FAILED@selectAllUsers");
        }
        return users;
    }

    public int validateUser(String username, String password) {
        /*The password needs to be hashed */
        int userid = -4;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String q = "SELECT * FROM " + TABLE_UTILIZADORES + " WHERE " +
                    TABLE_UTILIZADORES_COL_USERNAME + " = ? AND " +
                    TABLE_UTILIZADORES_COL_PASSWORD + " = ?";
            String filters[] = new String[]{username, password};

            Cursor c = db.rawQuery(q, filters);

            int resultCount = c.getCount();
            if (resultCount == 0) {
                userid = -1; //no match
            } else if (resultCount > 1) {
                userid = -2; //cannot have repeated usernames
            } else if (resultCount == 1) {
                c.moveToFirst();
                userid = c.getInt(c.getColumnIndex(TABLE_UTILIZADORES_COL_ID));
            } else {
                userid = -3; //WTF??
            }

            db.close();
        } catch (Exception e) {
            Log.e(this.getClass().getName(), e.toString() + " FAILED@getUserId");
        }

        return userid;
    }

    public long insertUser(String username, String password) {
        long idOndeFoiFeitoInsert = -1;

        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues pairs = new ContentValues();
            pairs.put(TABLE_UTILIZADORES_COL_USERNAME, username);
            pairs.put(TABLE_UTILIZADORES_COL_PASSWORD, Utils.md5(password));

            idOndeFoiFeitoInsert = db.insert(TABLE_UTILIZADORES, null, pairs);

            db.close();
            return idOndeFoiFeitoInsert;
        } catch (Exception e) {
            //SQLiteException if the database cannot be opened for writing
            return -1; //FAIL!
        }
    }

    public int updateUserPassword(String username, String newPassword){
        int rowsAffected = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues pairs = new ContentValues();
            pairs.put("password",Utils.md5(newPassword));
            String filters[] = new String[]{username};
            rowsAffected = db.update(TABLE_UTILIZADORES,pairs,"username = ?",filters);
            db.close();
        } catch (Exception e) {
            return rowsAffected;
        }
        return rowsAffected;
    }

    public int removeUser(String username){
        int rowsAffected = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            String[] filters = new String[]{username};
            rowsAffected = db.delete(TABLE_UTILIZADORES,"username = ?",filters);
            db.close();
        }catch (Exception e) {
            return rowsAffected;
        }
        return rowsAffected;
    }
}