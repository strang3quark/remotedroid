(function() {

  let phoneAddr; //ip addr + port of the phone (baseurl)
  let credentials; //credentials object
  let listBox; //The thread list
  let listBoxContent; //The message content list
  let writePanel; //The message write panel
  let noConversation; //The no conversation selected message
  let txtMsg; //The message textarea
  let notifiedList; //Array with ids of notified messages
  let audioNotification;

  let currentScreen;
  const CURR_SCREEN_SMS = 0;
  const CURR_SCREEN_CONTACTS = 1;

  window.addEventListener('load', init);
  window.addEventListener('resize', setDimensions);
  
  function init(){
  	console.log("App Started at: "+phoneAddr);

    phoneAddr = window.location.host;
    $('#btn_login').click(login);
    $('#btn_send').click(btnSendListener);
    txtMsg = $('#txt_content');
    writePanel = $("#message_write_panel");
    noConversation = $("#text_nothingselected");
    currentScreen = CURR_SCREEN_SMS;
  
    listBox = new ListBox("listbox");
    listBoxContent = new ListBox("listbox_content");
  
    setDimensions(); //adjust the size of the elements
    audioNotification = new Audio('assets/audio/newmessage.mp3');
  }
  
  /* Login function, called on #btn_login click
      If the credentials are correct show the sms interface
  */
  function login(){
	var passhash = CryptoJS.MD5($("#inputPassword").val());
    credentials = {
      user:$("#inputUsername").val(),
      pass:passhash,
      ip:phoneAddr
    }
    doRequest(credentials,'/auth',function(jsonObject){
      if (jsonObject['validauth'] === "true"){
        console.log("Lets Go!");
        $("#pane-login").hide();
        $("#pane-sms").show();
        doRequest(credentials, '/sms/threads', populateThreadListWithMessages);
        notifiedList = new Array();
        var interval = setInterval(checkUnreadMessages,5000);
      }
    });
  }

  function contactListClickListener(){
    listBox.setSelectedObject(this);
    var mContactAddress = listBox.getSelectedObject().address;
    var mContactId = listBox.getSelectedObject().contactId;

    doRequest(
        credentials,
        '/sms/thread',
        populateThreadContent,
        {
            contactid:mContactId,
            start: 0,
            end: 30
        }
    );

    noConversation.hide();
    writePanel.show();
    setDimensions();
  }
  /* Opens a conversation
      Called when a user clicks on a conversation
  */
  function threadListClickListener(){
    console.log(this);
    listBox.setSelectedObject(this);
    var mThreadId = listBox.getSelectedObject().threadId; //get the conversation id
    doRequest(credentials, '/sms/thread', populateThreadContent,{
      threadid:mThreadId,
      start:0,
      end:30
    });
    noConversation.hide();
    writePanel.show();
    setDimensions();
  }
  
  /* Send a message and update the sms interface
    Called when clicked on #btn_send
  */
  function btnSendListener(){
    var mAddress = listBox.getSelectedObject().address;
    var mContent = txtMsg.val();
    doRequest(credentials, '/sms/send', function(){
        txtMsg.val("");
        sleep(2500).then(()=>{
          if (currentScreen == CURR_SCREEN_SMS){
            var mThreadId = listBox.getSelectedObject().threadId; //get the conversation id
            //update the thread list and conversation
            doRequest(credentials, '/sms/thread', populateThreadContent,{
              threadid:mThreadId,
              start:0,
              end:30
            });
            //TODO: temporary fix, need to select by threadId
            listBox.selectedIndex = 1; //the selected thread will move to top
            doRequest(credentials, '/sms/threads', populateThreadListWithMessages);
          }else if (currentScreen == CURR_SCREEN_CONTACTS) {
              var mContactId = listBox.getSelectedObject().contactId;
              //update the conversation
              doRequest(credentials, '/sms/thread', populateThreadContent, {
                contactid:mContactId,
                start:0,
                end:30
              });
          }
        });
    }, {address:mAddress, content:mContent});
  }

  function populateThreadListWithContacts(jsonObject){
    listBox.clear();
    listBox.count = 0;
    listBox.selectedIndex = -1;
    currentScreen = CURR_SCREEN_CONTACTS;

    btnBack = listBox.addItem("Back");
    btnBack.className += " list-group-item-danger";
    btnBack.addEventListener('click', function(){
        listBoxContent.clear();
        listBox.selectedIndex = -1;
        doRequest(credentials, '/sms/threads', populateThreadListWithMessages);

        noConversation.show();
        writePanel.hide();
    });

    jsonObject.forEach(function(contact) {
        if (contact.phonenr != undefined){
            item = listBox.addItem(contact.name,contact.phonenr);
            item.address = contact.phonenr;
            item.contactId = contact.id;
            item.addEventListener('click', contactListClickListener);
        }
    }, true);
  }

  /* Add a list of items to the threadList #listbox */
  function populateThreadListWithMessages(jsonObject){
    var oldSelectedIndex = listBox.selectedIndex;
    listBox.clear();
    listBox.count = 0;
    listBox.selectedIndex = -1;
    currentScreen = CURR_SCREEN_SMS;

    btnNewThread = listBox.addItem("New Thread");
    btnNewThread.className += " list-group-item-success";
    btnNewThread.addEventListener('click', function(){
        listBoxContent.clear();
        doRequest(credentials, '/contacts', populateThreadListWithContacts);
        noConversation.show();
        writePanel.hide();
    });
  
    jsonObject.forEach(function(thread) {
     item = listBox.addItem(thread.name + " ("+thread.msgcount+")",thread.snippet);
     item.threadId = thread.thread_id;
     item.address = thread.address;
     item.addEventListener('click',threadListClickListener);
    }, this);


    if (oldSelectedIndex > -1){
        listBox.setSelectedIndex(oldSelectedIndex); //make item ACTIVE
    }
  }
  
  /* Add a list of items to the Conversation window #listbox_content */
  function populateThreadContent(jsonObject){
    listBoxContent.clear();
    jsonObject.forEach(function(message){
      item = listBoxContent.addItem(message.content);
      if (message.type == 1){
        item.className += " list-group-item-success";
      }
    }, this)
    setDimensions();
  }
  
  function doRequest(cred,uri,callback,query){
	  var qs = generateQueryString(query);
	  var settings = {
		    "async": true,
		    "crossDomain": true,
      		"url": "http://"+cred.ip+uri+qs,
		    "method": "GET",
		  	"dataType": "text",
		    "headers": {
				    "auth-user": cred.user,
				    "auth-password": cred.pass,
				  }
	  }

    console.log("Requesting: "+settings.url);
	  $.ajax(settings).done(function (response) {
		  	var info = JSON.parse(response);
		    console.log(info);
		  	callback(info);
	  });
  }

  function generateQueryString(jsonObject){
	if (jsonObject == null){
		return "";
	}
	var resp = "?";
	$.each(jsonObject, function(key,value){
		resp += key+"="+encodeURIComponent(value)+"&";
	});
	
	return resp;
  }

  function checkUnreadMessages(){
    //TODO: use mContactId when currScreen == CURR_SCREEN_CONTACTS
    var mThreadId = -1
    var mContactId = -1;
    if (listBox.selectedIndex > -1 && currentScreen == CURR_SCREEN_SMS){
        mThreadId = listBox.getSelectedObject().threadId; //get the conversation id
    }else if (listBox.selectedIndex > -1 && currentScreen == CURR_SCREEN_CONTACTS){
        mContactId = listBox.getSelectedObject().contactId;
    }

    var newMessages = false;
    var refreshThisConversation = false;
    doRequest(credentials, '/sms/unread', function(jsonObject){
        jsonObject.forEach(function(message){
            var isAlreadyNotified = notifiedList.indexOf(message.msg_id) > -1;
            if (!isAlreadyNotified){
                newMessages = true;
                notifiedList.push(message.msg_id);

                if (message.thread_id != mThreadId && message.contact_id != mContactId){
                    sendMessageNotification(message.name,message.content,message.msg_id,message.thread_id);
                }else{
                    refreshThisConversation = true;
                }
            }
        }); //forEach

        if (newMessages){
          audioNotification.play();
          if (currentScreen == CURR_SCREEN_SMS){
            doRequest(credentials, '/sms/threads', populateThreadListWithMessages);
            if (refreshThisConversation == true){
              doRequest(credentials, '/sms/thread', populateThreadContent,{
                  threadid:mThreadId,
                  start:0,
                  end:30}
              );
            }
          } else if (currentScreen == CURR_SCREEN_CONTACTS){
            if (refreshThisConversation == true) {
              doRequest(credentials, '/sms/thread', populateThreadContent,{
                  contactid:mContactId,
                  start:0,
                  end:30}
              );
            }
          }
        }
    }); //doRequest
  }

  function sendMessageNotification(title,content,msgId,threadId){
    $.notify({
        title: '<strong>'+title+'</strong><br>',
        message: content
    },{
        type:'success',
        template: '<div id="notify-thread-'+msgId+'" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                   	'<span data-notify="icon"></span> ' +
                   	'<span data-notify="title">{1}</span> ' +
                   	'<span data-notify="message">{2}</span>' +
                   	'<div class="progress" data-notify="progressbar">' +
                   		'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                   	'</div>' +
                   	'<a href="{3}" target="{4}" data-notify="url"></a>' +
                  '</div>'
    });

    $('#notify-thread-'+msgId).click(function(){
        doRequest(credentials, '/sms/thread', populateThreadContent,{threadid:threadId, start:0, end:30});
    });
  }
  
  function setDimensions(){
    var h = $(window).height();
    var lbContent = $("#listbox_content");
    lbContent.height(h-130);
    lbContent.scrollTop(lbContent.prop("scrollHeight"));
    $("#listbox").height(h-40);
  }
  
  function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }
})();
