function ListBox(id){
    this.object = document.getElementById(id)
    this.object.className += "list-group";
    this.selectedIndex = -1
    this.count = 0
    this.getTitle = (index) => {return this.object.children[index].children[0].innerText}
    this.getDescription = (index) => {return this.object.children[index].children[1].innerText}
    this.getSelectedObject = () => { return this.object.children[this.selectedIndex]}
    this.getSelectedTitle = () => {return this.getTitle[this.selectedIndex]}
    this.getSelectedDescription = () => {return this.getDescription[this.selectedIndex]}
    //this.getSelectedTitle = () => { return this.getSelectedObject().children[0].innerText }
    //this.getSelectedDescription = () => { return this.getSelectedObject().children[0].innerText }
    this.setSelectedIndex = (index) => {
      if (this.selectedIndex > -1){
        var currentSelected = this.getSelectedObject();
        currentSelected.className = "list-group-item"; //remove active
      }
      this.selectedIndex = index;
      var newSelected = this.getSelectedObject();
      newSelected.className = "list-group-item active";
    }
    this.setSelectedObject = (obj) => {
      var objId = obj.id;
      var objIndex = obj.id.substring(obj.id.indexOf("-")+1);

      this.setSelectedIndex(objIndex);
    }
    this.addItem = (title, description) => {
      let id = "idListItem-"+this.count;

      let oItem = document.createElement('a');
      oItem.id = id;
      oItem.index = this.count;
      oItem.className = "list-group-item";
  
      if (description !== undefined){
        let oTitle = document.createElement('h4');
        oTitle.className = "list-group-item-heading";
        oTitle.innerText = title;

        let oDescription = document.createElement('p');
        oDescription.className = "list-group-item-text";
        oDescription.innerText = description

        oItem.appendChild(oTitle);
        oItem.appendChild(oDescription);
      }else{
        oItem.innerText = title;
      }

      this.object.appendChild(oItem);

      this.count++;
      return oItem;
    }
    this.clear = () => {this.object.innerHTML = ""}
}