# RemoteDroid

**Send SMS from your android smartphone using your computer**

This project consists of an API with and embed JavaScript client

This was a university project and is still a work in progress

**Caution:** HTTPS suport is not yet implemented, use at your own risk in public networks.

## Screenshots

### Client
![BrowserApp](http://i.imgur.com/sdj35cy.png)

### Android
![ServerApp](https://i.imgur.com/ly9eUls.png)

# How to Use
First you need to create a new user, you can do it by clicking on the menu (on the Android Phone).

After the user has been created you just need to open your favorite webbrowser and put your phone address (shown before "Running At")

# How to Install
At the moment there isn't any compiled APKs and it isn't available on PlayStore, I will take care of that when the software is more stable.

You just need to clone the repo and open in Android Studio.

# The API
You can use the API without the "internal" client, this means you can use your own client or some scripts with CURL.

## Authentication
The RemoteDroid checks credentials passed via these HTTP Headers:
 + **auth-user**: The username
 + **auth-password**: The md5 hashed password

All responses are sent in JSON, you can make request using GET or POST (it's up to you).

**Later I will put JSON examples of all responses**

## SMS Methods
### /sms/threads
**Gets a list of conversations**

    [
      {
        "address": "+351931112233",
        "contact_id": "152",
        "thread_id": "1",
        "name": "Maria",
        "snippet": "How are you doing",
        "msgcount": "579"
      },
      {
        "address": "+14154888663",
        "thread_id": "2",
        "name": "+14154888663",
        "snippet": "The day's High is 16C with a Low of 4C. Current conditions are Rain.",
        "msgcount": "13"
      },
      {
        "address": "911111111",
        "contact_id": "56",
        "thread_id": "5",
        "name": "Micael Castelo",
        "snippet": "I'm ready in 10 minutes.",
        "msgcount": "9"
      },
      {
        "address": "Operator",
        "thread_id": "12",
        "name": "Operator",
        "snippet": "You received your payment for this month",
        "msgcount": "1"
      },
    ]

**The name will be equal to address if it isn't in phonebook**

### /sms/thread*?threadid=5&start=0&end=3*
**Gets the messages in a conversation, the above example gets the last 3 messages**

    [
      {
        "address": "911 111 111",
        "contact_id": "56",
        "thread_id": "5",
        "msg_id": "75",
        "date": "1486023802722",
        "name": "Micael Castelo",
        "content": "Ok",
        "type": "2"
      },
      {
        "address": "+351911111111",
        "contact_id": "56",
        "thread_id": "5",
        "msg_id": "36",
        "date": "1486117585500",
        "name": "Micael Castelo",
        "content": "I'm waiting",
        "type": "1"
      },
      {
        "address": "911 111 111",
        "contact_id": "56",
        "thread_id": "5",
        "msg_id": "35",
        "date": "1486117822425",
        "name": "Micael Castelo",
        "content": "I'm ready in 10 minutes.",
        "type": "2"
      }
    ]

**Type:** 
+ 1 - sent 
+ 2 - received

### /sms/unread
**Gets a list of unread messages**

    [
     {
      "address": "+35191211211",
      "contact_id": "102",
      "thread_id": "13",
      "msg_id": "207",
      "date": "1485438453668",
      "name": "Ruben Anjos",
      "content": "So? ",
      "type": "1"
     },
     {
      "address": "Pingo Doce",
      "thread_id": "10",
      "msg_id": "848",
      "date": "1486494796708",
      "name": "Pingo Doce",
      "content": "Bla bla bla, some marketing message",
      "type": "1"
     }
    ]

### /sms/send*?address=00351911234567&content=Hello*
**Send an SMS message**

    {
     "SENDSMS": "DONE"
    }

### /sms/send/status
**Get the status of the last sent SMS**

    {
     "STATUS": "SENT"
    }
    
OR

    {
     "STATUS": "ERROR"
    }

## Contacts
### /contacts/get*?id=2*
**Get contact by ID**

    {
     "phonenr": "+35193245333",
     "name": "Jack",
     "id": "2"
    }

### /contacts/get*?address=93245333*
**Get contact by address**

    {
     "phonenr": "+35193245333",
     "name": "Jack",
     "id": "2"
    }

# The Client
The native client is the one you see in the screenshot above

## Current Client Limitations
 + Unable to create new conversations with unknown numbers
 + Can only see last 30 messages per thread
 + Probably has a lot of bugs
